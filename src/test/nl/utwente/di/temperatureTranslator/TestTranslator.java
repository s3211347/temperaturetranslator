package nl.utwente.di.temperatureTranslator;

import main.nl.utwente.di.temperatureTranslator.Translator;
import org.junit.jupiter.api.Assertions ;
import org.junit.jupiter.api.Test;

/**
 * Test class for Translator.
 */
public class TestTranslator {
    @Test
    public void testBook1 ( ) throws Exception {
    Translator translator = new Translator();
    double temperature = translator.getFahrenheits(0.0);
    Assertions.assertEquals(32.0, temperature,0.0, "0 Celsius to Fahrenheit");
    }
}