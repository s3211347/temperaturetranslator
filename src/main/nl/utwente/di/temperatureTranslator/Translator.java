package main.nl.utwente.di.temperatureTranslator;

public class Translator {
    public double  getFahrenheits(Double celsius) {
        return (celsius * 9 / 5) + 32;
    }
}
